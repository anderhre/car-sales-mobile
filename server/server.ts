import typeDefs from './typedefs';
import resolvers from './resolvers';
import { makeExecutableSchema } from '@graphql-tools/schema';
import IP from '../IP';
const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const cors = require('cors');
const mongoose = require('mongoose');

// Create schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

// Create server and enable cross-origin
const server = express();
server.use(cors());
server.use(
  '/graphql',
  graphqlHTTP({
    schema,
    graphiql: true,
  }),
);

// Connect to database
const uri = 'mongodb://localhost:27017/cars_db';
if (IP.length !== 0){
mongoose
  .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    if (mongoose.connection.readyState == 1) {
      console.log('DB connected');
      //server.listen(4000);
      server.listen({host: IP, port:4000});
      console.log('Server listening on port 4000');
    }
  })
  .catch((e: String) => {
    console.log(e);
  });
} else {
  console.error('IP IS NOT DEFINED, insert IP address in server/server.ts line 9.');
}
