const mongoose = require('mongoose');

const Cars = mongoose.model("Cars", {
    index: { type: Number },
    price: { type: Number },
    model: { type: String },
    brand: { type: String },
    year: { type: Number },
    title_status: { type: String },
    mileage: { type: Number },
    color: { type: String },
    vin: { type: String },
    lot: { type: Number },
    state: { type: String },
    country: { type: String },
    condition: { type: String },
    likes: { type: Number }
  });

  export default Cars;