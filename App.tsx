import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import Header from './components/Header';
import CarList from './components/CarList';
import { DataStoreProvider } from './context';
import SERVER_IP from './IP';

if (SERVER_IP.length === 0) {
  console.error('SERVER_IP IS NOT DEFINED, insert IP address in App.tsx line 10.');
}

const client = new ApolloClient({
  uri: 'http://' + SERVER_IP + ':4000/graphql',
});

const App = () => {
  return (
    <DataStoreProvider>
      <ApolloProvider client={client}>
        <View style={styles.app}>
          <Header title="Car Sales" />
          <CarList />
          <StatusBar style="auto" />
        </View>
      </ApolloProvider>
    </DataStoreProvider>
  );
};

const styles = StyleSheet.create({
  app: {
    flex: 1,
    backgroundColor: '#212529',
  },
});

export default App;
