import React, { useEffect, useState } from 'react';
import gql from 'graphql-tag';
import { useLazyQuery } from 'react-apollo';
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Button,
  ActivityIndicator,
  StyleSheet,
  Keyboard,
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import ListItem from './ListItem';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { observer } from 'mobx-react-lite';
import { useDataStore } from '../context';

const INPUT_HEIGHT = 40;

const CARS_QUERY = gql`
  query CarsQuery(
    $brand: String
    $model: String
    $minYear: Int
    $maxYear: Int
    $minPrice: Int
    $maxPrice: Int
    $sortField: SortableField!
    $sortOrder: SortOrder
    $page: Int
  ) {
    cars(
      filter: {
        brand: $brand
        model: $model
        minyear: $minYear
        maxyear: $maxYear
        minprice: $minPrice
        maxprice: $maxPrice
        page: $page
      }
      sort: { field: $sortField, order: $sortOrder }
    ) {
      cars {
        index
        price
        model
        brand
        year
        title_status
        mileage
        color
        vin
        lot
        state
        country
        condition
        likes
      }
      finalpage
    }
  }
`;

interface Car {
  index: number;
  price: number;
  model: String;
  brand: String;
  year: number;
  title_status: String;
  mileage: number;
  color: String;
  vin: String;
  lot: number;
  state: string;
  country: String;
  condition: String;
  likes: number;
}

// Default sorting values
let sortField = 'brand';
let sortOrder = 'ASC';
const FIRSTPAGE = 1;

const CarList = observer(() => {
  const store = useDataStore();
  const [brand, setBrand] = useState('');
  const [model, setModel] = useState('');
  const [minYear, setMinYear] = useState(1973);
  const [maxYear, setMaxYear] = useState(2020);
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(100000);
  const [page, setPage] = useState(1);
  const [finalPage, setFinalPage] = useState(0);
  const [getCars, { data, loading }] = useLazyQuery(CARS_QUERY);

  AsyncStorage.getItem('darkmode').then((value) => {
    if (value === 'true' || null) {
      store.setDarkTheme();
    }
  });

  // Handle click on page-buttons
  function handlePageClick(page: number) {
    getCars({
      variables: {
        brand,
        model,
        minYear,
        maxYear,
        minPrice,
        maxPrice,
        sortField,
        sortOrder,
        page,
      },
    });

    // If final page-button was clicked
    if (page === -1) {
      setPage(finalPage);
    } else {
      setPage(page);
    }
  }

  // Handle click on table-headers
  function handleHeaderClick(header: string) {
    sortField = header;

    // Change sort order
    sortOrder === 'ASC' ? (sortOrder = 'DESC') : (sortOrder = 'ASC');

    search();
  }

  const search = () => {
    setPage(1);
    getCars({
      variables: {
        brand,
        model,
        minYear,
        maxYear,
        minPrice,
        maxPrice,
        sortField,
        sortOrder,
        FIRSTPAGE,
      },
    });
  };

  useEffect(() => {
    if (data !== undefined) {
      setFinalPage(data.cars.finalpage);
    }
  }, [data]);

  useEffect(() => {
    search();
  }, [minYear, maxYear]);

  return (
    <>
      <View style={styles.labelBar}>
        <Text style={styles.inputLabel}>{'Brand'}</Text>
        <Text style={styles.inputLabel}>{'Min price'}</Text>
        <Text style={styles.inputLabel}>{'Min model year'}</Text>
      </View>
      <View style={styles.inputBar}>
        <TextInput
          placeholder="  Brand"
          placeholderTextColor={store.darkTheme ? '#949494' : '#8e8e8e'}
          onChangeText={(text) => setBrand(text)}
          returnKeyType="search"
          onSubmitEditing={search}
          style={[
            styles.textInput,
            store.darkTheme ? styles.darkBackground : styles.lightBackground,
          ]}
        />
        <TextInput
          value={minPrice.toString()}
          onChangeText={(text) => setMinPrice(Math.floor(+text))}
          keyboardType="decimal-pad"
          returnKeyType="search"
          onSubmitEditing={search}
          style={[
            styles.textInput,
            store.darkTheme ? styles.darkBackground : styles.lightBackground,
          ]}
        />
        <View
          style={[
            styles.comboBoxInput,
            store.darkTheme ? styles.darkBackground : styles.lightBackground,
          ]}
        >
          <Picker
            selectedValue={minYear}
            onValueChange={(value) => setMinYear(+value)}
            style={styles.comboBox}
          >
            {getYearOptions(1973, maxYear)}
          </Picker>
        </View>
      </View>
      <View style={styles.labelBar}>
        <Text style={styles.inputLabel}>{'Model'}</Text>
        <Text style={styles.inputLabel}>{'Max price'}</Text>
        <Text style={styles.inputLabel}>{'Max model year'}</Text>
      </View>
      <View style={styles.inputBar}>
        <TextInput
          placeholder="  Model"
          placeholderTextColor={store.darkTheme ? '#949494' : '#8e8e8e'}
          onChangeText={(text) => setModel(text)}
          returnKeyType="search"
          onSubmitEditing={search}
          style={[
            styles.textInput,
            store.darkTheme ? styles.darkBackground : styles.lightBackground,
          ]}
        />
        <TextInput
          value={maxPrice.toString()}
          onChangeText={(text) => setMaxPrice(Math.floor(+text))}
          keyboardType="decimal-pad"
          returnKeyType="search"
          onSubmitEditing={search}
          style={[
            styles.textInput,
            store.darkTheme ? styles.darkBackground : styles.lightBackground,
          ]}
        />
        <View
          style={[
            styles.comboBoxInput,
            store.darkTheme ? styles.darkBackground : styles.lightBackground,
          ]}
        >
          <Picker
            selectedValue={maxYear}
            onValueChange={(value) => setMaxYear(+value)}
            style={styles.comboBox}
          >
            {getYearOptions(minYear, 2020)}
          </Picker>
        </View>
      </View>
      <View style={styles.searchButtonContainer}>
        <View style={styles.searchButton}>
          <Button
            title="Search"
            onPress={() => {
              search();
              Keyboard.dismiss();
            }}
            color={store.darkTheme ? '#580173' : '#0b90e3'}
          />
        </View>
      </View>
      <View style={styles.columnHeaders}>
        <TouchableOpacity style={styles.columnHeader} onPress={() => handleHeaderClick('brand')}>
          <Text
            style={[styles.columnHeaderText, store.darkTheme ? styles.textDark : styles.textLight]}
          >
            Brand
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.columnHeader} onPress={() => handleHeaderClick('model')}>
          <Text
            style={[styles.columnHeaderText, store.darkTheme ? styles.textDark : styles.textLight]}
          >
            Model
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.columnHeader}
          onPress={() => {
            handleHeaderClick('price');
          }}
        >
          <Text
            style={[styles.columnHeaderText, store.darkTheme ? styles.textDark : styles.textLight]}
          >
            Price
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.columnHeader} onPress={() => handleHeaderClick('year')}>
          <Text
            style={[styles.columnHeaderText, store.darkTheme ? styles.textDark : styles.textLight]}
          >
            Year
          </Text>
        </TouchableOpacity>
      </View>
      <ScrollView
        style={[
          styles.scrollView,
          store.darkTheme ? styles.scrollDarkBackground : styles.lightBackground,
        ]}
      >
        {data === undefined && loading !== true && (
          <View style={styles.infoTextContainer}>
            <Text style={styles.infoText}>US Car sales database!</Text>
            <Text style={styles.infoText}>Search to view cars for sale.</Text>
          </View>
        )}
        {data !== undefined && data.cars.cars.length === 0 && (
          <Text style={styles.infoText}>No cars match the search</Text>
        )}
        {data !== undefined &&
          data.cars.cars.map(function (car: Car, index: number) {
            return <ListItem key={index} props={car} />;
          })}
      </ScrollView>
      {data !== undefined && data.cars.cars.length !== 0 && (
        <View style={styles.pagination}>
          {page !== 1 && (
            <>
              <TouchableOpacity
                style={[
                  styles.paginationButton,
                  store.darkTheme ? styles.paginationButtonDark : styles.paginationButtonLight,
                ]}
                onPress={() => handlePageClick(1)}
              >
                <Text style={styles.paginationText}>{'<<'}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.paginationButton,
                  store.darkTheme ? styles.paginationButtonDark : styles.paginationButtonLight,
                ]}
                onPress={() => handlePageClick(page - 1)}
              >
                <Text style={styles.paginationText}>{'<'}</Text>
              </TouchableOpacity>
            </>
          )}
          <TouchableOpacity
            style={[
              styles.paginationButton,
              store.darkTheme ? styles.paginationButtonDark : styles.paginationButtonLight,
            ]}
          >
            <Text
              style={[
                styles.paginationText,
                store.darkTheme ? styles.paginationTextDark : styles.paginationTextLight,
              ]}
            >
              {page}
            </Text>
          </TouchableOpacity>
          {page !== finalPage && (
            <>
              <TouchableOpacity
                style={[
                  styles.paginationButton,
                  store.darkTheme ? styles.paginationButtonDark : styles.paginationButtonLight,
                ]}
                onPress={() => handlePageClick(page + 1)}
              >
                <Text style={styles.paginationText}>{'>'}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.paginationButton,
                  store.darkTheme ? styles.paginationButtonDark : styles.paginationButtonLight,
                ]}
                onPress={() => handlePageClick(finalPage)}
              >
                <Text style={styles.paginationText}>{'>>'}</Text>
              </TouchableOpacity>
            </>
          )}
        </View>
      )}
      {loading === true && <ActivityIndicator size="large" color="#0b90e3" />}
    </>
  );
});

//contentContainerStyle={{ flexGrow: 1 }}

const styles = StyleSheet.create({
  // Labels
  labelBar: {
    paddingTop: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputLabel: {
    flex: 1,
    textAlign: 'center',
    color: 'white',
  },

  // Input
  inputBar: {
    paddingTop: 5,
    flexDirection: 'row',
    height: INPUT_HEIGHT,
    marginLeft: 10,
    marginRight: 10,
  },
  textInput: {
    flex: 1,
    borderWidth: 1,
    marginRight: 5,
  },
  darkBackground: {
    backgroundColor: '#32373d',
  },
  lightBackground: {
    backgroundColor: 'white',
  },
  scrollDarkBackground: {
    backgroundColor: '#40454a',
  },
  comboBoxInput: {
    flex: 1,
    borderWidth: 1,
  },
  comboBox: {
    flex: 1,
    color: 'black',
  },

  // Search button
  searchButtonContainer: {
    paddingTop: 12,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  searchButton: {
    width: 100,
  },

  // Column headers
  columnHeaders: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 8,
  },
  columnHeader: {
    flex: 1,
    alignItems: 'center',
    padding: 5,
  },
  columnHeaderText: {
    fontSize: 18,
    backgroundColor: '#40454a',
    borderRadius: 3,
    width: 70,
    height: 30,
    paddingTop: 2,
    textAlign: 'center',
  },

  // Table
  scrollView: {
    flex: 1,
    maxHeight: 435,
  },

  // Pagination
  pagination: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    margin: 5,
  },
  paginationButton: {
    borderWidth: 1,
    textAlign: 'center',
    height: 35,
    width: 40,
  },
  paginationButtonDark: {
    backgroundColor: '#580173',
  },
  paginationButtonLight: {
    backgroundColor: '#0b90e3',
  },
  paginationText: {
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
  },
  paginationTextDark: {
    color: 'black',
  },
  paginationTextLight: {
    color: 'black',
  },
  // Info text (no cars are displayed)
  infoTextContainer: {
    paddingTop: 30,
  },
  infoText: {
    textAlign: 'center',
    fontSize: 16,
  },

  textDark: {
    color: 'black',
    fontWeight: 'bold',
  },
  textLight: {
    color: 'white',
  },
});

// Get options for Picker (combo-boxes)
function getYearOptions(start: number, end: number) {
  let options = [];

  for (let i = start; i <= end; i++) {
    options.push(<Picker.Item key={i} label={i.toString()} value={i} />);
  }
  return options;
}

export default CarList;
