import React, { useState } from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-async-storage/async-storage';
import gql from 'graphql-tag';
import { useMutation } from 'react-apollo';
import { useDataStore } from '../context';
import { observer } from 'mobx-react-lite';

interface Car {
  props: {
    index: number;
    price: number;
    brand: String;
    model: String;
    year: number;
    title_status: String;
    mileage: number;
    color: String;
    vin: String;
    lot: number;
    state: String;
    country: String;
    condition: String;
    likes: number;
  };
}

const LIKE_MUTATION = gql`
  mutation RatingMutation($id: Int!, $likeAmount: Int!) {
    like(id: $id, likeAmount: $likeAmount) {
      success
    }
  }
`;

const ListItem = observer(({ props }: Car) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [hasLiked, sethasLiked] = useState(false);
  const [insertLike] = useMutation(LIKE_MUTATION);

  const index = props.index.toString();

  AsyncStorage.getItem(index).then((value) => {
    sethasLiked(value === 'liked');
  });

  const store = useDataStore();

  async function like(like: boolean) {
    if (like) {
      await AsyncStorage.setItem(index, 'liked');
    } else {
      await AsyncStorage.setItem(index, 'disliked');
    }

    let id = parseInt(index);
    let likeAmount = like ? 1 : -1;

    insertLike({
      variables: {
        id,
        likeAmount,
      },
    });

    props.likes += likeAmount;
  }

  return (
    <>
      <TouchableOpacity onPress={() => setModalVisible(true)}>
        <View style={styles.row}>
          <Text style={styles.text}>{props.brand}</Text>
          <Text style={styles.text}>{props.model}</Text>
          <Text style={styles.text}>{props.price}</Text>
          <Text style={styles.text}>{props.year}</Text>
        </View>
      </TouchableOpacity>

      <Modal
        isVisible={modalVisible}
        onBackButtonPress={() => setModalVisible(false)}
        onBackdropPress={() => setModalVisible(false)}
      >
        <View
          style={[
            styles.modal,
            store.darkTheme ? styles.modalDarkBackground : styles.modalLightBackground,
          ]}
        >
          <Text style={styles.modalHeadline}>{props.brand + ' ' + props.model}</Text>
          <View
            style={[styles.modalRow, store.darkTheme ? styles.modalRowDark : styles.modalRowLight]}
          >
            <Text style={styles.rowIdentifier}>{'Location: '}</Text>
            <Text style={styles.modalText}>{props.country}</Text>
          </View>
          <View
            style={[styles.modalRow, store.darkTheme ? styles.modalRowDark : styles.modalRowLight]}
          >
            <Text style={styles.rowIdentifier}>{'Mileage: '}</Text>
            <Text style={styles.modalText}>{props.mileage}</Text>
          </View>
          <View
            style={[styles.modalRow, store.darkTheme ? styles.modalRowDark : styles.modalRowLight]}
          >
            <Text style={styles.rowIdentifier}>{'Year: '}</Text>
            <Text style={styles.modalText}>{props.year}</Text>
          </View>
          <View
            style={[styles.modalRow, store.darkTheme ? styles.modalRowDark : styles.modalRowLight]}
          >
            <Text style={styles.rowIdentifier}>{'Color: '}</Text>
            <Text style={styles.modalText}>{props.color}</Text>
          </View>
          <View
            style={[styles.modalRow, store.darkTheme ? styles.modalRowDark : styles.modalRowLight]}
          >
            <Text style={styles.rowIdentifier}>{'VIN: '}</Text>
            <Text style={styles.modalText}>{props.vin}</Text>
          </View>
          <View style={styles.likesRow}>
            <Text style={styles.likesText}>{'Likes: '}</Text>
            {hasLiked && <Text style={styles.likesBadgeGreen}>{props.likes}</Text>}
            {!hasLiked && (
              <Text
                style={[
                  styles.likesBadgeGrey,
                  store.darkTheme ? styles.likesBadgeGreyDark : styles.likesBadgeGreyLight,
                ]}
              >
                {props.likes}
              </Text>
            )}
          </View>
          <View style={styles.buttonRow}>
            <View style={styles.button}>
              {hasLiked && <Button title="Dislike" onPress={() => like(false)} color={'red'} />}
              {!hasLiked && <Button title="Like" onPress={() => like(true)} color={'green'} />}
            </View>
            <View style={styles.button}>
              <Button
                title="Close"
                onPress={() => setModalVisible(false)}
                color={store.darkTheme ? '#0f0f0f' : '#0b90e3'}
              />
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
});

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    borderBottomWidth: 1,
  },
  text: {
    flex: 1,
    borderBottomColor: 'black',
    textAlign: 'center',
    fontSize: 16,
    color: 'black',
  },

  // Modal
  modal: {
    margin: 10,
  },
  modalDarkBackground: {
    backgroundColor: '#212529',
  },
  modalLightBackground: {
    backgroundColor: 'white',
  },
  modalHeadline: {
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    borderBottomWidth: 1,
    color: 'black',
  },

  likesRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginLeft: 15,
    marginBottom: 5,
  },
  likesText: {
    fontSize: 16,
  },
  likesBadgeGreen: {
    width: 25,
    height: 25,
    borderRadius: 10,
    backgroundColor: 'green',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  likesBadgeGrey: {
    width: 25,
    height: 25,
    borderRadius: 10,
    textAlign: 'center',
    fontSize: 18,
    color: 'black',
  },
  likesBadgeGreyDark: {
    backgroundColor: '#40454a',
  },
  likesBadgeGreyLight: {
    backgroundColor: 'grey',
  },
  rowIdentifier: {
    marginLeft: 15,
    fontWeight: 'bold',
    fontSize: 16,
  },
  modalRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 45,
    borderBottomWidth: 1,
  },
  modalRowDark: {
    backgroundColor: '#40454a',
  },
  modalRowLight: {
    backgroundColor: 'grey',
  },
  modalText: {
    borderBottomColor: 'black',
    fontSize: 16,
  },

  buttonRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    flex: 1,
  },
});

export default ListItem;
