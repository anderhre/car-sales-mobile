import React, { FC, useContext, useState } from 'react';
import { View, Text, Switch, StyleSheet, Platform, StatusBar } from 'react-native';
import { Left, Body, Right } from 'native-base';
//import { DarkThemeStoreContext } from '../stores/darkThemeStore';
import { observer } from 'mobx-react-lite';
import { useDataStore } from '../context';
import Constants from 'expo-constants';

interface HeaderProps {
  title: String;
}

const Header = observer(({ title }: HeaderProps) => {
  const store = useDataStore();
  const [darkTheme, setIsDarkTheme] = useState(store.darkTheme);

  return (
    <View style={[styles.header, store.darkTheme ? styles.darkHeader : styles.lightHeader]}>
      <Left style={styles.left}></Left>
      <Body style={styles.body}>
        <Text style={styles.text}>{title}</Text>
      </Body>
      <Right style={styles.right}>
        <Text style={styles.rightText}>{'Dark mode'}</Text>
        <Switch
          trackColor={{ false: 'white', true: '#5e5e5e' }}
          thumbColor={store.darkTheme ? 'black' : 'white'}
          onValueChange={() => {
            darkTheme ? store.setLightTheme() : store.setDarkTheme();
            setIsDarkTheme(!darkTheme); // Switch needs state to update properly
          }}
          value={darkTheme}
        ></Switch>
      </Right>
    </View>
  );
});

let statusBarHeight = 0;
if (Platform.OS !== 'ios' && StatusBar.currentHeight) {
  statusBarHeight = StatusBar.currentHeight;
}

const styles = StyleSheet.create({
  header: {
    height: 40 + statusBarHeight,
    flexDirection: 'row',
    paddingTop: Platform.OS === 'ios' ? 0 : statusBarHeight,
  },
  darkHeader: {
    backgroundColor: '#580173',
  },
  lightHeader: {
    backgroundColor: '#0b90e3',
  },
  left: {
    flex: 4,
  },
  body: {
    flex: 6,
  },
  right: {
    flex: 4,
    flexDirection: 'row',
    paddingTop: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightText: {
    fontSize: 12,
    color: 'black',
  },
  text: {
    fontSize: 22,
    textAlign: 'center',
    color: 'white',
  },
});

export default Header;
